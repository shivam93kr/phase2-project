package com.learnersacademy.DAO;

import java.util.List;

import com.model.pojo.Student;
import com.model.pojo.Subject;
import com.model.pojo.Teacher;

public interface TeacherDAO {
	public Teacher createTeacher(Teacher teacher);
	public Teacher getTeacherById(int id);
	public List<Teacher> getAllTeacherList();
	public void removeTeacher(int id);
	public Teacher updateTeacher(Teacher teacher);
	public List<Subject> updateSubject(Teacher teacher);
	public List<Student> updateStudent(Teacher teacher);
	
}
