package com.learnersacademy.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.learnersacademy.service.SubjectService;
import com.learnersacademy.service.impl.SubjectServiceImpl;
import com.model.pojo.Subject;

@Path("/assign")
public class AssignClassController {
	
	SubjectService service = new SubjectServiceImpl();
	
	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Subject updateSubject(Subject subject) {
		return service.updateSubject(subject);
	}
}
