package com.learnersacademy.service.impl;

import java.util.List;

import com.learnersacademy.DAO.StudentDAO;
import com.learnersacademy.DAO.impl.StudentDAOImpl;
import com.learnersacademy.service.StudentService;
import com.model.pojo.Student;

public class StudentServiceImpl implements StudentService{
	
	StudentDAO dao = new StudentDAOImpl();
	@Override
	public Student createStudent(Student student) {
		// TODO Auto-generated method stub
		return dao.createStudent(student);
	}

	@Override
	public Student getStudentById(int id) {
		// TODO Auto-generated method stub
		return dao.getStudentById(id);
	}

	@Override
	public List<Student> getAllStudentList() {
		// TODO Auto-generated method stub
		return dao.getAllStudentList();
	}

	@Override
	public void removeStudent(int id) {
		// TODO Auto-generated method stub
		dao.removeStudent(id);
	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		return dao.updateStudent(student);
	}
	
	

}
